
// TestProject1Dlg.h : header file
//

#pragma once

#include "KinectController.h"

// CTestProject1Dlg dialog
class CTestProject1Dlg : public CDialogEx
{
// Construction
public:
	CTestProject1Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TESTPROJECT1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	static DWORD WINAPI ShowThread(LPVOID lpParam);
	DWORD WINAPI ShowCase();
public:
	afx_msg void OnBnClickedOk();
	afx_msg void PauseStream();

private: 
	KinectController kController;
	bool pause;
	HANDLE hShowCaseThread;
	HANDLE *streamMutex;
};
