
// TestProject1.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTestProject1App:
// See TestProject1.cpp for the implementation of this class
//

class CTestProject1App : public CWinApp
{
public:
	CTestProject1App();
	
// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTestProject1App theApp;