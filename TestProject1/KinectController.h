// Class that controls kinect connection and Image streaming
// Establishes connection and open color, depth and IR stream
//

#pragma once

#include "stdafx.h"
#include "OpenCVHelper.h"

#include <NuiApi.h>

class KinectController {

public:

	/// Constructor
	KinectController();
	/// Destructor
	~KinectController();
	
	void OpenStream();
	void PauseStream();

	void GetMatrices(Mat* colorMat, Mat* depthMat);

	HANDLE * GetStreamMutex();
	
	HRESULT GetColorImage(Mat* colorImage);

	HRESULT GetDepthImage(Mat* depthImage);

	HRESULT GetDepthImageAsArgb(Mat* depthImage);

	void GetColorImageSize(DWORD *width, DWORD *height);

	int GetColorTYPE();
private:

	void StartKinect();

	//Initializes default resolution of streamed image
	void InitResolution();

	void InitMatrices();
	
	/// <summary>
    /// Initializes the first available Kinect found
    /// </summary>
    /// <returns>S_OK if successful, E_FAIL otherwise</returns>
	HRESULT CreateFirstConnected();
	
	DWORD WINAPI OpenCVThread();
	static DWORD WINAPI OpenStreamThread(LPVOID lpParam);

	// Variables:
	// Helpers
	Microsoft::KinectBridge::OpenCVFrameHelper frameHelper;
	OpenCVHelper m_openCVHelper;

	// App settings
	bool m_bIsColorPaused;
	NUI_IMAGE_RESOLUTION m_colorResolution;
	int m_colorFilterID;

	bool m_bIsDepthPaused;
	bool m_bIsDepthNearMode;
	NUI_IMAGE_RESOLUTION m_depthResolution;
	int m_depthFilterID;

	bool m_bIsSkeletonDrawColor;
	bool m_bIsSkeletonDrawDepth;

	//// Frame rate tracking
	//FrameRateTracker m_colorFrameRateTracker;
	//FrameRateTracker m_depthFrameRateTracker;

	// OpenCV matrices
	Mat m_colorMat;
	Mat m_depthMat;

	//// Bitmaps
	//BITMAPINFO m_bmiColor;
	//void* m_pColorBitmapBits;
	//HBITMAP m_hColorBitmap;

	//BITMAPINFO m_bmiDepth;
	//void* m_pDepthBitmapBits;
	//HBITMAP m_hDepthBitmap;

	// Window processing thread handles
	HANDLE m_hProcessStopEvent;
	HANDLE m_hProcessThread;

	// Mutexes that control access to m_colorResolution and m_depthResolution
	HANDLE m_hColorResolutionMutex;
	HANDLE m_hDepthResolutionMutex;

	// Mutexes that control access to m_hColorBitmap and m_hDepthBitmap
	HANDLE m_hColorBitmapMutex;
	HANDLE m_hDepthBitmapMutex;

	HANDLE m_hShowStreamMutex;

	// Debug boolean
	bool m_bIsConnected;
	bool m_bContinueStream;
};
