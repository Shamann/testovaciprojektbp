#include "stdafx.h"
#include "OpenCVHelper.h"


using namespace cv;

const Scalar OpenCVHelper::SKELETON_COLORS[NUI_SKELETON_COUNT] =
{
    Scalar(255, 0, 0),      // Blue
    Scalar(0, 255, 0),      // Green
    Scalar(64, 255, 255),   // Yellow
    Scalar(255, 255, 64),   // Light blue
    Scalar(255, 64, 255),   // Purple
    Scalar(128, 128, 255)   // Pink
};

/// <summary>
/// Constructor
/// </summary>
OpenCVHelper::OpenCVHelper() :
    m_depthFilterID(-1),
    m_colorFilterID(-1)
{
}

/// <summary>
/// Sets the color image filter to the one corresponding to the given resource ID
/// </summary>
/// <param name="filterID">resource ID of filter to use</param>
void OpenCVHelper::SetColorFilter(int filterID)
{
    m_colorFilterID = filterID;
}

/// <summary>
/// Sets the depth image filter to the one corresponding to the given resource ID
/// </summary>
/// <param name="filterID">resource ID of filter to use</param>
void OpenCVHelper::SetDepthFilter(int filterID)
{
    m_depthFilterID = filterID;
}

/// <summary>
/// Applies the color image filter to the given Mat
/// </summary>
/// <param name="pImg">pointer to Mat to filter</param>
/// <returns>S_OK if successful, an error code otherwise
HRESULT OpenCVHelper::ApplyColorFilter(Mat* pImg)
{
    // Fail if pointer is invalid
    if (!pImg) 
    {
        return E_POINTER;
    }

    // Fail if Mat contains no data
    if (pImg->empty()) 
    {
        return E_INVALIDARG;
    }

    // Apply an effect based on the active filter
    switch(m_colorFilterID)
    {
    case IDM_COLOR_FILTER_GAUSSIANBLUR:
        {
            GaussianBlur(*pImg, *pImg, Size(7,7), 0);
        }
        break;
    case IDM_COLOR_FILTER_DILATE:
        {
            dilate(*pImg, *pImg, Mat());
        }
        break;
    case IDM_COLOR_FILTER_ERODE:
        {
            erode(*pImg, *pImg, Mat());
        }
        break;
    case IDM_COLOR_FILTER_CANNYEDGE:
        {
            const double minThreshold = 30.0;
            const double maxThreshold = 50.0;

            // Convert image to grayscale for edge detection
            cvtColor(*pImg, *pImg, CV_RGBA2GRAY);
            // Remove noise
            blur(*pImg, *pImg, Size(3,3));
            // Find edges in image
            Canny(*pImg, *pImg, minThreshold, maxThreshold);
            // Convert back to color for output
            cvtColor(*pImg, *pImg, CV_GRAY2RGBA);
        }
        break;
    }

    return S_OK;
}

/// <summary>
/// Applies the depth image filter to the given Mat
/// </summary>
/// <param name="pImg">pointer to Mat to filter</param>
/// <returns>S_OK if successful, an error code otherwise</returns>
HRESULT OpenCVHelper::ApplyDepthFilter(Mat* pImg)
{
    // Fail if pointer is invalid
    if (!pImg) 
    {
        return E_POINTER;
    }

    // Fail if Mat contains no data
    if (pImg->empty()) 
    {
        return E_INVALIDARG;
    }

    // Apply an effect based on the active filter
    switch(m_depthFilterID)
    {
    case IDM_DEPTH_FILTER_GAUSSIANBLUR:
        {
            GaussianBlur(*pImg, *pImg, Size(5,5), 0);
        }
        break;
    case IDM_DEPTH_FILTER_DILATE:
        {
            dilate(*pImg, *pImg, Mat());
        }
        break;
    case IDM_DEPTH_FILTER_ERODE:
        {
            erode(*pImg, *pImg, Mat());
        }
        break;
    case IDM_DEPTH_FILTER_CANNYEDGE:
        {
            const double minThreshold = 5.0;
            const double maxThreshold = 20.0;

            // Convert image to grayscale for edge detection
            cvtColor(*pImg, *pImg, CV_RGBA2GRAY);
            // Remove noise
            blur(*pImg, *pImg, Size(3,3));
            // Find edges in image
            Canny(*pImg, *pImg, minThreshold, maxThreshold);
            // Convert back to color for output
            cvtColor(*pImg, *pImg, CV_GRAY2RGBA);
        }
        break;
    }

    return S_OK;
}
