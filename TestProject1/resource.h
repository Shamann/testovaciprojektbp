//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestProject1.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTPROJECT1_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDM_COLOR_FILTER_GAUSSIANBLUR   164
#define IDM_COLOR_FILTER_DILATE         165
#define IDM_COLOR_FILTER_ERODE          166
#define IDM_COLOR_FILTER_CANNYEDGE      167
#define IDM_DEPTH_FILTER_GAUSSIANBLUR   176
#define IDM_DEPTH_FILTER_DILATE         177
#define IDM_DEPTH_FILTER_ERODE          178
#define IDM_DEPTH_FILTER_CANNYEDGE      179
#define IDC_BUTTON1                     1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
