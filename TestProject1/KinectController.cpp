#include "stdafx.h"
#include "KinectController.h"
#include <opencv2\opencv.hpp>
using namespace std;

KinectController::KinectController() :
    m_bIsColorPaused(false),
    m_colorResolution(NUI_IMAGE_RESOLUTION_INVALID),
    m_bIsDepthPaused(false),
    m_bIsDepthNearMode(false),
    m_depthResolution(NUI_IMAGE_RESOLUTION_INVALID),
    m_bIsSkeletonDrawColor(false),
    m_bIsSkeletonDrawDepth(false),
    m_hProcessStopEvent(NULL),
    m_hProcessThread(NULL),
    m_hColorResolutionMutex(NULL),
    m_hDepthResolutionMutex(NULL),
    m_hColorBitmapMutex(NULL),
    m_hDepthBitmapMutex(NULL),
	m_bIsConnected(false),
	m_bContinueStream(false)
{
	StartKinect();
};

KinectController::~KinectController() {

    if (m_hColorBitmapMutex) {
        CloseHandle(m_hColorBitmapMutex);
    }

    if (m_hDepthBitmapMutex) {
        CloseHandle(m_hDepthBitmapMutex);
    }

	if (m_hShowStreamMutex) {
		CloseHandle(m_hShowStreamMutex);
	}

};

void KinectController::StartKinect() {

	// Create mutexes
	m_hColorResolutionMutex = CreateMutex(NULL, FALSE, NULL);
	m_hDepthResolutionMutex = CreateMutex(NULL, FALSE, NULL);
	m_hColorBitmapMutex = CreateMutex(NULL, FALSE, NULL);
	m_hDepthBitmapMutex = CreateMutex(NULL, FALSE, NULL);
	m_hShowStreamMutex = CreateMutex(NULL, FALSE, NULL);

	// Startup initialization
	InitResolution();
	InitMatrices();

	if (SUCCEEDED(CreateFirstConnected())) {
		m_bIsConnected = true;
	};
}

void KinectController::OpenStream() {

	if (m_bIsConnected) {
		if (!m_bContinueStream) {
			m_bContinueStream = true;
			m_hProcessStopEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
			m_hProcessThread = CreateThread(NULL, 0, OpenStreamThread, this, 0, NULL);
			
		}
	}	
}

void KinectController::PauseStream() {
	m_bContinueStream = false;
}

DWORD WINAPI KinectController::OpenStreamThread(LPVOID lpParam) {
	KinectController *pThis = reinterpret_cast<KinectController*>(lpParam);
	return pThis->OpenCVThread();
}

DWORD WINAPI KinectController::OpenCVThread() {

	HANDLE hEvents[3] = {m_hProcessStopEvent, NULL, NULL};
    int numEvents;
    if (frameHelper.IsInitialized()) {
        frameHelper.GetColorHandle(hEvents + 1);
        frameHelper.GetDepthHandle(hEvents + 2);
        numEvents = 3;
    }
    else {
        numEvents = 1;
    }


	while(m_bContinueStream) {

		//Update image output
		if (frameHelper.IsInitialized()) {

			// Update color frame
				if (!m_bIsColorPaused && SUCCEEDED(frameHelper.UpdateColorFrame())) {
					WaitForSingleObject(m_hShowStreamMutex, INFINITE);
					HRESULT hr = frameHelper.GetColorImage(&m_colorMat);
					ReleaseMutex(m_hShowStreamMutex);
					if (FAILED(hr)) {
						continue;
					}

					// Apply filter to color stream
					// hr = m_openCVHelper.ApplyColorFilter(&m_colorMat);
					//if (FAILED(hr))
					//{
					//    return 1;
					//}
				}

				//WaitForSingleObject(m_hColorBitmapMutex,INFINITE);
				//cv::imshow("Color",m_colorMat);
				//ReleaseMutex(m_hColorBitmapMutex);

				if (!m_bIsDepthPaused && SUCCEEDED(frameHelper.UpdateDepthFrame())) {
					WaitForSingleObject(m_hShowStreamMutex, INFINITE);
					HRESULT hr = frameHelper.GetDepthImageAsArgb(&m_depthMat);
					ReleaseMutex(m_hShowStreamMutex);
					if (FAILED(hr)) {
						continue;
					}

					//Apply filter to depth stream
				}

				//WaitForSingleObject(m_hDepthBitmapMutex,INFINITE);
				//cv::imshow("Depth",m_depthMat);
				//ReleaseMutex(m_hDepthBitmapMutex);
		}
	}
	return 0;
}

HRESULT KinectController::CreateFirstConnected() {

	HRESULT hr;
	INuiSensor * sensor = NULL;

	
	// Get number of Kinect sensors
    int sensorCount = 0;
    hr = NuiGetSensorCount(&sensorCount);
    if (FAILED(hr)) 
    {
        return hr;
    }

	// If no sensors, return failure
	if (0 == sensorCount) {
		return E_FAIL;
	}

	// Iterate through Kinect sensors until one is successfully initialized
    for (int i = 0; i < sensorCount; ++i) 
    {
        hr = NuiCreateSensorByIndex(i, &sensor);
        if (SUCCEEDED(hr))
        {
            hr = frameHelper.Initialize(sensor);
            if (SUCCEEDED(hr)) 
            {
                // Report success
                return S_OK;
            }
            else
            {
                // Uninitialize KinectHelper to show that Kinect is not ready
                frameHelper.UnInitialize();
            }
        }
    }

    // Report failure
    return E_FAIL;
};

void KinectController::InitResolution() {
	m_colorResolution = NUI_IMAGE_RESOLUTION_640x480;
    frameHelper.SetColorFrameResolution(m_colorResolution);

	m_depthResolution = NUI_IMAGE_RESOLUTION_640x480;
	frameHelper.SetDepthFrameResolution(m_depthResolution);
}

void KinectController::InitMatrices() {

	DWORD width, height;
    frameHelper.GetColorFrameSize(&width, &height);

    Size size(width, height);
    m_colorMat.create(size, frameHelper.COLOR_TYPE);

	frameHelper.GetDepthFrameSize(&width, &height);

	size.height = height;
	size.width = width;

	m_depthMat.create(size, frameHelper.DEPTH_RGB_TYPE);
	
}

HRESULT KinectController::GetColorImage(Mat* colorImage) {
	return frameHelper.GetColorImage(colorImage);
};

HRESULT KinectController::GetDepthImage(Mat* depthImage) {
	return frameHelper.GetDepthImage(depthImage);
};

HRESULT KinectController::GetDepthImageAsArgb(Mat* depthImage) {
	return frameHelper.GetDepthImageAsArgb(depthImage);
};

void KinectController::GetColorImageSize(DWORD *width, DWORD *height) {
	frameHelper.GetColorFrameSize(width, height);
};

int KinectController::GetColorTYPE() {
	return frameHelper.COLOR_TYPE;
};

void KinectController::GetMatrices(Mat *colorMat, Mat *depthMat) {
	*colorMat = m_colorMat;
	*depthMat = m_depthMat;
};

HANDLE * KinectController::GetStreamMutex() {
	return &m_hShowStreamMutex;
};