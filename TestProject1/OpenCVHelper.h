//-----------------------------------------------------------------------------
// <copyright file="OpenCVHelper.h" company="Microsoft">
//     Copyright (c) Microsoft Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------------

#pragma once

#include "resource.h"
#include <Windows.h>
#include <NuiApi.h>

// OpenCV includes
// Suppress warnings that come from compiling OpenCV code since we have no control over it
#pragma warning(push)
#pragma warning(disable : 6294 6031)
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#pragma warning(pop)

#include "OpenCVFrameHelper.h"

using namespace cv;

class OpenCVHelper
{
    // Constants:
    // Skeleton colors for each player index
    static const Scalar SKELETON_COLORS[NUI_SKELETON_COUNT];

public:
    /// <summary>
    /// Constructor
    /// </summary>
    OpenCVHelper();

    /// <summary>
    /// Sets the color image filter to the one corresponding to the given resource ID
    /// </summary>
    /// <param name="filterID">resource ID of filter to use</param>
    void SetColorFilter(int filterID);

    /// <summary>
    /// Sets the depth image filter to the one corresponding to the given resource ID
    /// </summary>
    /// <param name="filterID">resource ID of filter to use</param>
    void SetDepthFilter(int filterID);

    /// <summary>
    /// Applies the color image filter to the given Mat
    /// </summary>
    /// <param name="pImg">pointer to Mat to filter</param>
    /// <returns>S_OK if successful, an error code otherwise
    HRESULT ApplyColorFilter(Mat* pImg);

    /// <summary>
    /// Applies the depth image filter to the given Mat
    /// </summary>
    /// <param name="pImg">pointer to Mat to filter</param>
    /// <returns>S_OK if successful, an error code otherwise</returns>
    HRESULT ApplyDepthFilter(Mat* pImg);


private:
    // Variables:
    // Resource IDs of the active filters
    int m_colorFilterID;
    int m_depthFilterID;
};